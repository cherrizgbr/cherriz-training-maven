package de.cherriz.training.maven;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {

    private Calculator calc;

    @Before
    public void setUp() {
        calc = new Calculator();
    }

    @Test
    public void plus() {
        Assert.assertEquals(calc.plus(2, 2), 4);
    }

    @Test
    public void minus() {
        Assert.assertEquals(calc.minus(2, 2), 0);
    }

    @Test
    public void multiply() {
        Assert.assertEquals(calc.multiply(2, 2), 4);
    }

    @Test
    public void divide() {
        Assert.assertEquals(calc.divide(2, 2), 1);
    }

}